#include <libtcod/libtcod.hpp>
#include "Map.hpp"
#include "Actor.hpp"
#include "Engine.hpp"

static const int ROOM_MAX_SIZE = 11;
static const int ROOM_MIN_SIZE = 5;

class BspListener : public ITCODBspCallback {
private:
  Map &map;
  int roomNum;
  int lastx, lasty;
public:
  BspListener(Map &map) : map(map), roomNum(0) {}
  bool visitNode(TCODBsp *node, void *userData) {
    if (node->isLeaf()) {
      int x, y, w, h; // Initializes x and y, width and height of room
      // dig room
      TCODRandom *rng = TCODRandom::getInstance();
      w = rng->getInt(ROOM_MIN_SIZE, node->w - 2);
      h = rng->getInt(ROOM_MIN_SIZE, node->h - 2);
      x = rng->getInt(node->x + 1, node->x + node->w - w - 1);
      y = rng->getInt(node->y + 1, node->y + node->h - h - 1);
      map.createRoom(roomNum == 0, x, y, x + w - 1, y + h - 1);
      if (roomNum != 0) {
	// dig corridor from last room
	map.dig(lastx, lasty, x + w / 2, lasty);
	map.dig(x + w / 2, lasty, x + w / 2, y + h / 2);
      }
      lastx = x + w / 2;
      lasty = y + h / 2;
      roomNum++;
    }
    return true;
  }
};

Map::Map(int width, int height) : width(width),height(height) {
    tiles = new Tile[width*height];
    map = new TCODMap(width, height);
    TCODBsp bsp(0, 0, width, height);
    bsp.splitRecursive(NULL, 8, ROOM_MAX_SIZE, ROOM_MAX_SIZE, 1.5f, 1.5f);
    BspListener listener(*this);
    bsp.traverseInvertedLevelOrder(&listener, NULL);
}

Map::~Map() {
    delete [] tiles;
    delete map;
}

bool Map::isWall(int x, int y) const {
  return !map->isWalkable(x, y);
}

bool Map::isExplored(int x, int y) const {
  return tiles[x + y * width].explored;
}

bool Map::isInFov(int x, int y) const {
  if(map->isInFov(x, y)) {
    tiles[x + y * width].explored = true;
    return true;
  }
  return false;
}

void Map::computeFov() {
  map->computeFov(engine.player->x, engine.player->y, engine.fovRadius);
}

void Map::render() const {
    static const TCODColor darkWall(46, 42, 46);
    static const TCODColor darkGround(0, 0, 0);
    static const TCODColor lightWall(130,110,50);
    static const TCODColor lightGround(200,180,50);

    for (int x=0; x < width; x++) {
        for (int y=0; y < height; y++) {
	  if (isInFov(x, y)) {
	    TCODConsole::root->setCharBackground(x, y,
		isWall(x, y) ? lightWall : lightGround);
	  } else if (isExplored(x, y)) {
            TCODConsole::root->setCharBackground(x, y,
                isWall(x,y) ? darkWall : darkGround);
	  }
        }
    }
}

void Map::dig(int firstX, int firstY, int secondX, int secondY) {
  if (secondX < firstX) {
    int tmp = secondX;
    secondX = firstX;
    firstX = tmp;
  }
  if (secondY < firstY) {
    int tmp = secondY;
    secondY = firstY;
    firstX = tmp;
  }
  for (int tilex = firstX; tilex <= secondX; tilex++) {
    for (int tiley = firstY; tiley <= secondY; tiley++) {
      map->setProperties(tilex, tiley, true, true);
    }
  }
}

void Map::createRoom(bool isFirst, int firstX, int firstY, int secondX, int secondY) {
  dig(firstX, firstY, secondX, secondY);
  if (isFirst) {
    // Put the player inside of the first room
    engine.player->x = (firstX + secondX) / 2;
    engine.player->y = (firstY + secondY) / 2;
  } else {
    TCODRandom *rng = TCODRandom::getInstance(); // Adds a random number generator
    if (rng->getInt(0, 3) == 0) {
      engine.actors.push(new Actor((firstX + secondX) / 2, (firstY + secondY) / 2, '@',
				   TCODColor::red));
    }
  }
}


