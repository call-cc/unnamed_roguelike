struct Tile {
	bool explored;
	Tile() : explored(false) {}
};
 
class Map {
public:
    int width,height;
 
    Map(int width, int height);
    ~Map();
    bool isWall(int x, int y) const;
	bool isInFov(int x, int y) const;
	bool isExplored(int x, int y) const;
	void computeFov();
 	void render() const;
protected:
    Tile *tiles;
	TCODMap *map;
	friend class BspListener;

	void dig(int firstX, int firstY, int secondX, int secondY);
	void createRoom(bool isFirst, int firstX, int firstY, int secondX, int secondY);
};
